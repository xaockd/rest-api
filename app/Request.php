<?php

namespace App;

use Jenssegers\MongoDb\Model as Eloquent;

class Request extends Eloquent
{
    const STATE_APPROVE    = 'approve';
    const STATE_REJECT     = 'reject';
    const STATE_UNRESOLVED = 'unresolved';

    protected $collection = 'request';

    protected $fillable = ['user_id', 'friend_id', 'state'];

    protected $hidden = [
        "created_at", "updated_at"
    ];

    public $incrementing = false;

}