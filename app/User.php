<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class User extends Eloquent
{
    protected $collection = "users";

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', "created_at", "updated_at"
    ];

    /**
     * @return bool
     */
    public function hasFriends()
    {
        return count($this->friends) > 0;
    }

    /**
     *
     * @param $user_id
     * @return bool
     */
    public function hasFriend($user_id)
    {
        return in_array($user_id, $this->friends);
    }

    /**
     * @param $friend_id
     * @return mixed
     */
    public function addFriend($friend_id)
    {
        return $this->push('friends', $friend_id, true);
    }

    /**
     * @param $friend_id
     * @return mixed
     */
    public function removeFriend($friend_id)
    {
        return $this->pull('friends', $friend_id);
    }

    /**
     * @param $value
     * @return array
     */
    public function getFriendsAttribute($value)
    {
        return is_array($value) ? $value : [];
    }

}
