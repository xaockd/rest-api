<?php

namespace App\Repository\Contracts;


interface Request
{
    /**
     * @param $user_id
     * @param $possible_friend_id
     * @return mixed
     */
    public function requestToBeFriend($user_id, $possible_friend_id);

    /**
     * @param $user_id
     * @param $state
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList($user_id, $state);

}