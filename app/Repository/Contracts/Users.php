<?php

namespace App\Repository\Contracts;


interface Users
{
    /**
     * @param $id
     * @return \App\User
     */
    public function findById($id);

    /**
     * @param $user
     * @param int $nesting
     * @return mixed
     */
    public function getFriends($user, $nesting = 0);
}