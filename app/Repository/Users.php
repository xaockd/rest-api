<?php

namespace App\Repository;

use \App\Repository\Contracts\Users as UsersContract;
use App\User;
use Illuminate\Support\Collection;

class Users implements UsersContract
{
    protected $friends;

    public function findById($id)
    {
        return User::findOrFail($id);
    }

    public function getFriends($user, $nesting = 0)
    {
        if(! $user->hasFriends()) {
            return new Collection();
        }

        if(! $this->friends) {
            $this->friends = new Collection();
        }

        $this->friends = $this->friends->merge(User::findMany($user->friends));

        while($nesting > 0) {

            //friends of friends
            $ids = $this->getFriendsIdsOfFriends();

            $this->friends = $this->friends->merge(User::findMany($ids));

            $nesting--;
        }

        return $this->friends->unique();
    }

    /**
     * Get friends ids of friends
     *
     * @return array
     */
    protected function getFriendsIdsOfFriends()
    {
        $friendsIds = $this->friends->pluck('friends');
        $ids = [];
        foreach($friendsIds as $friendIds) {
            $ids = array_merge($ids, $friendIds);
        }

        return array_unique($ids);
    }
}