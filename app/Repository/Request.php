<?php

namespace App\Repository;

use App\Repository\Contracts\Request as RequestContract;
use App\Request as RequestModel;

class Request implements RequestContract
{
    public function requestToBeFriend($user_id, $friend_id)
    {
        $request = RequestModel::firstOrNew([
            'user_id' => $user_id,
            'friend_id' => $friend_id,
        ]);

        $request->state = 'unresolved';

        $request->save();

        return $request;
    }

    /**
     * Get list of request to add user like a friend
     *
     * @param $user_id
     * @param string $state
     * @return mixed
     */
    public function getList($user_id, $state = 'unresolved')
    {
        return RequestModel::where('friend_id', '=', $user_id)->where('state', '=', $state)->get();
    }

    /**
     * Get list of request which user send
     *
     * @param $user_id
     * @return mixed
     */
    public function getMyRequest($user_id)
    {
        return RequestModel::where('user_id', '=', $user_id)->get();
    }

    /**
     * Find user
     *
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return RequestModel::findOrFail($id);
    }

}