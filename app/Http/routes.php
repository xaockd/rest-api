<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome', ['users' => \App\User::all()]);
});

Route::group(['middleware' => ['api']], function () {

    Route::group(['prefix' => 'friends'], function() {
        Route::get('/list/{level?}', ['as' => 'user.friends', 'uses' => 'UsersController@getFriends'])->where('level', '\d+');
        Route::post('/add/{user}', ['as' => 'user.request.make', 'uses' => 'UsersController@postAddFriend']);
        Route::post('/remove/{user}', ['as' => 'remove.friend', 'uses' => 'UsersController@postRemoveFriend']);
    });

    Route::group(['prefix' => 'requests'], function() {
        Route::get('/list', ['as' => 'user.requests', 'uses' => 'RequestsController@getRequestFriend']);
        Route::post('/approve/{request}', ['as' => 'user.requests.approve', 'uses' => 'RequestsController@postApproveRequest']);
        Route::post('/reject/{request}', ['as' => 'user.requests.reject', 'uses' => 'RequestsController@postRejectRequest']);
    });
});
