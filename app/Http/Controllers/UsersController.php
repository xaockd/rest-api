<?php

namespace App\Http\Controllers;


use App\Repository\Contracts\Request as RequestRepo;
use App\Repository\Contracts\Users as UsersRepo;
use App\User;
use Illuminate\Contracts\Auth\Guard;

class UsersController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|\App\User
     */
    protected $user;

    /**
     * @var RequestRepo
     */
    protected $requestRepo;

    /**
     * @var UsersRepo
     */
    protected $userRepo;

    /**
     * @param Guard $guard
     * @param RequestRepo $requestRepo
     * @param UsersRepo $userRepo
     */
    public function __construct(Guard $guard, RequestRepo $requestRepo, UsersRepo $userRepo)
    {
        $this->user = $guard->user();

        $this->requestRepo = $requestRepo;

        $this->userRepo = $userRepo;
    }

    /**
     * Make request add to friend
     *
     * @param User $friend
     * @return mixed
     */
    public function postAddFriend(User $friend)
    {
        if($this->user->hasFriend($friend->id)){
            return $this->makeResponse('fail', ['message' => 'User already in the friend list']);
        }

        $this->requestRepo->requestToBeFriend($this->user->id, $friend->id);

        return $this->makeResponse('success');
    }


    /**
     * Remove user from friend list
     *
     * @param User $friend
     * @return mixed
     */
    public function postRemoveFriend(User $friend)
    {
        $this->user->removeFriend($friend->id);

        return $this->makeResponse();
    }

    /**
     * @param int $nesting 1 -> friends of friends; 2 -> friends of friends of friends etc
     * @return mixed
     */
    public function getFriends($nesting = 0)
    {
        $friends = $this->userRepo->getFriends($this->user, $nesting);

        return $this->makeResponse('success', $friends);
    }

}