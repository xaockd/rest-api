<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Build response
     *
     * @param string $status
     * @param array $data
     * @return array
     */
    protected function makeResponse($status = 'success', $data = [])
    {
        $response = ['status' => $status];

        if($data) {
            $response['data'] = $data;
        }

        return $response;
    }
}
