<?php

namespace App\Http\Controllers;

use App\Repository\Contracts\Request as RequestRepo;
use App\Repository\Contracts\Users as UsersRepo;
use App\Request;
use App\User;
use Illuminate\Contracts\Auth\Guard;

class RequestsController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|\App\User
     */
    protected $user;

    /**
     * @var RequestRepo
     */
    protected $requestRepo;

    /**
     * @var UsersRepo
     */
    protected $userRepo;

    /**
     * @param Guard $guard
     * @param RequestRepo $requestRepo
     * @param UsersRepo $userRepo
     */
    public function __construct(Guard $guard, RequestRepo $requestRepo, UsersRepo $userRepo)
    {
        $this->user = $guard->user();

        $this->requestRepo = $requestRepo;

        $this->userRepo = $userRepo;
    }

    /**
     * Get request to add as friend
     *
     * @return mixed
     */
    public function getRequestFriend()
    {
        $request = $this->requestRepo->getList($this->user->id, Request::STATE_UNRESOLVED);

        return $this->makeResponse('success', $request->toArray());
    }

    /**
     * Approve request be friend
     *
     * @param $request
     * @return string
     */
    public function postApproveRequest(Request $request)
    {
        $request->state = Request::STATE_APPROVE;

        $request->save();

        $user = $this->userRepo->findById($request->user_id);

        $user->addFriend($request->friend_id);

        return $this->makeResponse();
    }

    /**
     * Reject request to be friend
     *
     * @param $request
     * @return string
     */
    public function postRejectRequest(Request $request)
    {
        $request->state = Request::STATE_REJECT;
        $request->save();

        return $this->makeResponse();
    }
}