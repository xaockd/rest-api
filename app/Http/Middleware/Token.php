<?php

namespace App\Http\Middleware;

use Closure;

class Token
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {

        if (!auth()->validate($request->all())) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
