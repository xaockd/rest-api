<?php

class UsersTest extends TestCase
{

    /**
     * Send request to add like a friend
     */
    public function testUserCanAddFriend()
    {
        $user =  factory(App\User::class)->create();
        $friend = factory(App\User::class)->create();

        $uri = sprintf('/friends/add/%s', $friend->id);

        $this->post($uri, ['api_token' => $user->api_token])->seeJsonEquals(['status' => 'success']);
    }

    /**
     * Remove from a friend list
     */
    public function testUserCanRemoveFriend()
    {
        $user =  factory(App\User::class)->create();
        $friend1 = factory(App\User::class)->create();
        $friend2 =  factory(App\User::class)->create();

        $user->addFriend($friend1->id);
        $user->addFriend($friend2->id);

        $this->assertEquals([$friend1->id, $friend2->id], $user->friends);

        $uri = sprintf('/friends/remove/%s', $friend1->id);
        $this->post($uri, ['api_token' => $user->api_token])->seeJsonEquals(['status' => 'success']);

        $this->assertEquals([$friend2->id], $user->fresh()->friends);
    }

    public function testFriendsList()
    {
        $user =  factory(App\User::class)->create();
        $friend1 = factory(App\User::class)->create();
        $friend2 =  factory(App\User::class)->create();

        $friend1_1 = factory(App\User::class)->create();
        $friend1_2 = factory(App\User::class)->create();
        $friend1_3 = factory(App\User::class)->create();

        $friend2_1 = factory(App\User::class)->create();

        $friend2_1_1 = factory(App\User::class)->create();

        $user->addFriend($friend1->id);
        $user->addFriend($friend2->id);

        $friend1->addFriend($friend1_1->id);
        $friend1->addFriend($friend1_2->id);
        $friend1->addFriend($friend1_3->id);

        $friend2->addFriend($friend2_1->id);
        $friend2_1->addFriend($friend2_1_1->id);

        $userFriends = $this->getUsersRepository()->getFriends($user);

        $this->assertEquals(2, count($userFriends));

        $userFriendsLevelOne= $this->getUsersRepository()->getFriends($user, 1);

        $this->assertEquals(6, count($userFriendsLevelOne));

        $userFriendsLevelTwo = $this->getUsersRepository()->getFriends($user, 2);
        $this->assertEquals(7, count($userFriendsLevelTwo));

        $userFriendsLevelFifty = $this->getUsersRepository()->getFriends($user, 50);
        $this->assertEquals(7, count($userFriendsLevelFifty));

    }

    /**
     * @return \App\Repository\Contracts\Request
     */
    public function getRequestRepository()
    {
        return app(App\Repository\Contracts\Request::class);
    }

    /**
     * @return \App\Repository\Contracts\Users
     */
    protected function getUsersRepository()
    {
        return app(App\Repository\Contracts\Users::class);
    }
}