<?php

class RequestsTest extends TestCase
{

    public function testShowRequests()
    {
        $user =  factory(App\User::class)->create();
        $follower1 = factory(App\User::class)->create();
        $follower2 = factory(App\User::class)->create();

        $this->getRequestRepository()->requestToBeFriend($follower1->id, $user->id);
        $this->getRequestRepository()->requestToBeFriend($follower2->id, $user->id);

        $requests = $this->getRequestRepository()->getList($user->id, \App\Request::STATE_UNRESOLVED)->toArray();

        $uri = '/requests/list?api_token=' . $user->api_token;

        $this->visit($uri)->seeJsonEquals(['status' => 'success', 'data' => $requests]);
    }


    public function testUserCanApproveFriendRequest()
    {
        $user =  factory(App\User::class)->create();
        $follower1 = factory(App\User::class)->create();
        $follower2 = factory(App\User::class)->create();

        $request1 = $this->getRequestRepository()->requestToBeFriend($follower1->id, $user->id);
        $this->getRequestRepository()->requestToBeFriend($follower2->id, $user->id);

        $approve_uri = sprintf( '/requests/approve/%s', $request1->id);

        $this->post($approve_uri, ['api_token' => $user->api_token])->seeJsonEquals(['status' => 'success']);

        $this->assertEquals(\App\Request::STATE_APPROVE, $request1->fresh()->state);

    }

    public function testUserCanRejectFriendRequest()
    {
        $user =  factory(App\User::class)->create();
        $follower1 = factory(App\User::class)->create();

        $request1 = $this->getRequestRepository()->requestToBeFriend($follower1->id, $user->id);

        $approve_uri = sprintf( '/requests/reject/%s', $request1->id);

        $this->post($approve_uri, ['api_token' => $user->api_token])->seeJsonEquals(['status' => 'success']);

        $this->assertEquals(\App\Request::STATE_REJECT, $request1->fresh()->state);
    }

    public function testFailWhenUseRequestTwice()
    {
        $user =  factory(App\User::class)->create();
        $follower1 = factory(App\User::class)->create();

        $request1 = $this->getRequestRepository()->requestToBeFriend($follower1->id, $user->id);

        $approve_uri = sprintf( '/requests/reject/%s', $request1->id);

        $this->post($approve_uri, ['api_token' => $user->api_token])
                ->post($approve_uri, ['api_token' => $user->api_token])
                ->seeStatusCode(404)
                ->seeJsonEquals(['status' => 'fail', ['message' => 'Model not found']]);
    }

    /**
     * @return \App\Repository\Contracts\Request
     */
    public function getRequestRepository()
    {
        return app(App\Repository\Contracts\Request::class);
    }

}