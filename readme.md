After create applicatino run in console php artisan db:seed


### Available methods.

Для запроса нужно взять токен пользователя, можно взять на главной странице.

1 Список друзей

```GET /friends/list/{mutual?}?api_token={token}```

mutual (int) - общие друзья, уровень вложености, по-умолчанию 0.

* друзья друзей

* друзья друзей друзей

2 Сделать запрос на добавления в друзья(одностороннее), токен в теле запроса.

```POST /friends/add/{user_id}``` api_token={token}

3 Отписатся от друга, токен в теле запроса.

```POST /friends/remove/{user_id}``` api_token={token}

4 Заявки на добавления в друзья

```GET /requests/list?api_token={token}```

5 Принять заявку, токен в теле запроса.

```POST /requests/approve/{request_id}``` api_token={token}

6 Отклонить заявку,  токен в теле запроса.

```POST /requests/reject/{request_id}``` api_token={token}


P.S. Некоторые ```POST``` запросы можно было бы заменить на семантически более правильные ```PUT``` ```DELETE```
